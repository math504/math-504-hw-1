Programming: computational cost of Gaussian elimination.

\subsection*{Problem 4.a}
The model eiffel1 was used to show the effect of perturbing the system by applying a horizontal force $ F^x_i $ with a magnitude of $ \frac{1}{5} $ at a node $i$. The perturbed linear system ($Ax=b$) was then solved using MatLab's backslash operator, which solves the linear system using Gaussian elimination with partial pivoting, where $A$ is the stiffness matrix, $b$ is a vector containing the external forces, and $x$ is the solution vector containing the unknown vertical and horizontal distance perturbations corresponding to each node. After solving the system the node positions were updated with the perturbations in the solution vector $x$. Figure \ref{fig:trussplot}, below, shows the initial unloaded system and the resulting loaded system, where the horizontal force is applied at the Node designated by the asterisk.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\textwidth]{Plot4a.png}
	\caption{Comparison of the unloaded and loaded eiffel tower from model eiffel1. 				The asterisk shows the location of the applied force on the tower.}
	\label{fig:trussplot}
\end{figure}

\subsection*{Problem 4.b}
The effect of the size $N$ of the linear system $Ax=b$ on the computational time of the Gaussian elimination algorithm was investigated by solving four different system of increasing $N$ (i.e. eiffel1, eiffel2, eiffel3, eiffel4) using MatLab's backslash operator. To get an accurate estimate on the total amount of time used by the cpu to perform the computation, each computation was performed roughly 100 times. The results for the four different systems are shown below in Figure \ref{fig:GE}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\textwidth]{Plot4b.png}
	\caption{CPU time as a function of N}
	\label{fig:GE}
\end{figure}

From theory the expected computational cost/time is expected to be $\mathcal{O}(n^3)$, however the experimental results indicate that the the computational cost  is closer to $\mathcal{O}(n^{2.6})$. This slight discrepancy between the theoretical and experimental result could be explained by MatLab's backslash operator not using strict Gaussian Elimination, but instead investigating the matrix $A$, and determining the best direct method for solving the problem.

\subsection*{Problem 4.c}
Gaussian elimination was again used to solve the four different systems and each one was solved $n$ different times, where only the external force vector $b$ was changing with each iteration. It was noticed that the stiffness matrix $A$ does not change with each new iteration, and therefore the LU factorization only needed to be performed once. Once the matrix $A$ was factored the system could be solved by repeatedly performing forward and backward substitution (i.e. $x=U\backslash (L\backslash b)$), which are much less costly computations then the factorization. Again each of the four different problems was run multiple times to obtain good statistics on the total computational time, the results are shown below in Table \ref{tab:GE}.

\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|l|}
		\hline
		Problem Name & Size (N) & CPU Time (sec) \\ \hline
		eiffel1		& 261		& 0.28		\\ \hline
		eiffel2		& 399		& 0.96		\\ \hline
		eiffel3		& 561		& 2.21		\\ \hline
		eiffel4		& 1592		& 47.67		\\ \hline
	\end{tabular}
	\caption{This table shows the total average computational time (i.e. LU 							factorization, and forward and backward substitution) for all four problems.}
	\label{tab:GE}
\end{table}

\subsection*{Problem 4.d}
Upon investigation of the stiffness matrix $A$, it was realized that the LU factorization could be significantly sped up since $A$ was sparse. After $A$ was declared sparse the same computation was performed from above by solving the system $N$ times for $N$ different force vectors. The results of the average CPU time for the four different systems are shown below in Table \ref{tab:SGE}.

\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|l|}
		\hline
		Problem Name & Size (N) & CPU Time (sec) \\ \hline
		eiffel1      & 261      & 0.029           \\ \hline
		eiffel2      & 399     & 0.073           \\ \hline
		eiffel3      & 561      & 0.18           \\ \hline
		eiffel4      & 1592    & 2.12          \\ \hline
	\end{tabular}
	\caption{This table shows the total average computational time (i.e. LU 							factorization, and forward and backward substitution) for all four problems 					when matrix $A$ is used as a sparse matrix.}
	\label{tab:SGE}
\end{table}

From Table \ref{tab:SGE}, it is clear that by realizing that matrix $A$ is sparse, a significant amount of computation time is saved. The amount of time saved is roughly $\frac{1}{10}$ the amount of time. It should be noted however, that the total cpu time for both methods is still roughly $\mathcal{O}(n^3)$. 