Let $U \in \mathbb{R}^{n \times n}$ be nonsingular upper triangular. We want to show that the backward substitution algorithm for solving $Ux=c$ is backward stable, i.e., the computed solution $\tilde{x}$ solves the nearby problem $\left(U+\delta U\right)\tilde{x}=c$, where $\left|\delta U\right| \leq n\varepsilon\left|U\right| \left(\textnormal{i.e.}\left|\delta u_{ij}\right|\leq n\varepsilon\left|u_{ij}\right|\right)$.

Note that backward substitution is an exact method, and we only need to consider round-off errors in the algorithm. 

Let $Ux=c$ be the exact problem. We want to find a small $\delta U$ such that the computed solution $\tilde{x}$ satisfies a nearby exact problem $\left(U+\delta U\right)\tilde{x}=c$. Setting $\tilde{U}:=U+\delta U$, we have $\tilde{U}\tilde{x}=c$.

The exact formula for backward substitution is
\begin{equation}
\label{star}
u_{jj}x_j=c_j-\sum_{i=j+1}^{n}u_{ji}x_i, \textnormal{ for }j=n,n-1,\ldots,1.
\end{equation}
The following two things are determined from this formula.
\begin{enumerate}
	\item $\tilde{x}$ is the exact solution to the nearby exact problem $\tilde{U}\tilde{x}=c$, i.e.,
	\begin{equation}
	\label{item1}
	\tilde{u}_{jj}\tilde{x}_j=c_j-\sum_{i=j+1}^{n}\tilde{u}_{ji}\tilde{x}_i, \textnormal{ for }j=n,n-1,\ldots,1.
	\end{equation}
	\item $\tilde{x}$ is also the computed solution of the exact problem using a computer with finite precision. We can obtain it by considering (\ref{star}) with round-off errors
	\begin{equation}
	\label{item2}
	\frac{u_{jj}\tilde{x}_j}{1+\delta}=c_j\left(1+\delta\right)^{n-j}-\sum_{i=j+1}^{n}u_{ji}\tilde{x}_i\left(1+\delta\right)^{n-i+2}, \textnormal{ for }j=n,n-1,\ldots,1.
	\end{equation}
\end{enumerate}

(\ref{item2}) was determined by analyzing the $j$-th step of the backward substitution algorithm. The algorithm can be broken up as follows:
\begin{enumerate}
	\item set $w^{(1)}:=c_j$
	\item do $w^{(i+1-j)}:=w^{(i-j)}-u_{ji}x_{i}$ for $i=j+1,\ldots,n$
	\item compute $x_j:=w^{(n+1-j)}/u_{jj}$
\end{enumerate}
Using the finite precision arithmetic model, we have for $i=j+1,\ldots,n$:
\[
\tilde{w}^{(i+1)}=\left(\tilde{w}^{(i)}-u_{ji}\tilde{x}_i\left(1+\delta_i\right)\right)\left(1+\delta'_i\right)
\]
and
\[
u_{jj}\tilde{x}_j=\tilde{w}^{(j)}\left(1+\delta_j\right).
\]
Putting this together we have 
\[
\frac{u_{jj}\tilde{x}_j}{1+\delta_j}=c_j\left(1+\delta'_{j+1}\right)\cdots\left(1+\delta'_{n}\right)-\sum_{i=j+1}^{n}u_{ji}\tilde{x}_i\left(1+\delta_i\right)\left(1+\delta'_i\right)\cdots\left(1+\delta'_n\right).
\]
Thus, we arrive at (\ref{item2}) when we substitute the generic $\delta$ into the above formula.

Solving for $c_j$ in (\ref{item1}) and (\ref{item2}) we find that
\begin{equation}
c_j=\sum_{i=j+1}^{n}\tilde{u}_{ji}\tilde{x}_i+\tilde{u}_{jj}\tilde{x}_j
\end{equation}
and
\begin{equation}
c_j=\sum_{i=j+1}^{n}u_{ji}\tilde{x}_i\left(1+\delta\right)^{j-i+2}+u_{jj}\tilde{x}_j\left(1+\delta\right)^{j-n-1}.
\end{equation}
By matching the coefficients in these sums, we see that
\begin{equation}
\tilde{u}_{ji}=u_{ji}\left(1+\delta\right)^{j-i+2}
\end{equation}
and
\begin{equation}
\tilde{u}_{jj}=u_{jj}\left(1+\delta\right)^{j-n-1}
\end{equation}
for $i=j+1,j+2,\ldots,n$ and $j=n,n-1,\ldots,1$.

Recall the following lemma from the lecture notes.
If $\left|\delta\right|\leq\varepsilon$, then we have $\left(1+\delta\right)^{-m}=1+\hat{\delta}$ where $\left|\hat{\delta}\right|\leq m\varepsilon +\mathcal{O}\left(\left(m\varepsilon\right)^2\right)$.

Since $\tilde{u}_{ji}=u_{ji}+\delta u_{ji}$, by the lemma, $u_{ji}+\delta u_{ji}=u_{ji}\left(1+\hat{\delta}\right)$ where $\left|\hat{\delta}\right|\leq \left(i-j-2\right)\varepsilon$, and, given all the possible values that $i$ and $j$ can assume, we find that $\left(i-j-2\right)$ is less than $n$. This means that $\left|\delta u_{ji}\right|\leq n\varepsilon\left|u_{ij}\right|$. Thus, $\left|\delta U\right|\leq n\varepsilon\left|U\right|$. Therefore, the backward substitution algorithm for solving $Ux=c$ is backward stable.