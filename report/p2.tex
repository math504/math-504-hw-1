Suppose $A \in \mathbb{R}^{n\times n}$ is lower triangular. We want to show that $A$ is nonsingular if and only if all diagonal elements of $A$ are non-zero.

First, we will prove that $A$ is nonsingular if all diagonal elements of $A$ are non-zero using a proof by contradiction.

Suppose a non-invertible $n \times n$ lower triangular matrix $A$ has non-zero entries on its main diagonal. Then, its rows are not linearly independent and its $k$-th row can be written as a linear combination of the other rows, i.e.,
\begin{equation}
A_{k,:}=\alpha_1A_{1,:}+\cdots+\alpha_{k-1}A_{k-1,:}+\alpha_{k+1}A_{k+1,:}+\cdots+\alpha_nA_{n,:}.
\end{equation}
Other rows below $A_{k,:}$, i.e., with indices $k+1,\ldots,n$, must have coefficients equal to zero. Particularly, $\alpha_n = 0$ because the $n$-th row has the only non-zero entry in the $n$-th column and $A_{k,:}$ has a zero entry in the $n$-th column. Thus,
\begin{equation}
A_{k,:}=\alpha_1A_{1,:}+\cdots+\alpha_{k-1}A_{k-1,:}+\alpha_{k+1}A_{k+1,:}+\cdots+\alpha_{n-1}A_{n-1,:}.
\end{equation}
A similar argument can be made for $\alpha_{n-1}$, $\alpha_{n-2}$, all the way down to $\alpha_{k+1}$, and we deduce that $\alpha_{k+1}=\cdots=\alpha_n=0$.
Consequently,
\begin{equation}
A_{k,:}=\alpha_1A_{1,:}+\cdots+\alpha_{k-1}A_{k-1,:}.
\end{equation}
This is a contradiction because $A_{k,:}$ has a non-zero entry in the $k$-th column and $A_{1,:},\ldots,A_{k-1,:}$ all have zero entries in that column. Thus, if all the diagonal entries of $A$ are non-zero, then no row of $A$ can be written as a linear combination of the others. Therefore, the rows are linearly independent and A is invertible.

Second, we will prove that $A$ is nonsingular only if all diagonal elements of $A$ are non-zero, again, using a proof by contradiction.

Suppose a $n \times n$ nonsingular lower triangular matrix $A$ has a zero entry on the $k$-th row of the main diagonal, i.e., $A_{kk}=0$. Now consider the $k \times n$ sub-matrix $B$ composed of the first $k$ rows of $A$. Since $A_{kk}=0$, the $k$-th column of $B$ is zero, and all the columns to its right are zero because $A$ is lower triangular. Hence, $B$ has at most $k-1$ non-zero columns, it also has at most $k-1$ linearly independent columns, and its column rank is at most $k-1$. This implies that $B$ has at most $k-1$ linearly independent rows because row and column rank coincide. Consequently, the $k$ rows of $B$ are not linearly independent, and since $B$ makes up a part of $A$, the rows of $A$ are not linearly independent. Thus, $A$ is not full rank and it is not invertible. This contradicts our original assumption that $A$ is nonsingular. Therefore, $A$ is nonsingular only if all diagonal elements of $A$ are non-zero.

This completes the proof that a lower triangular matrix $A$ is nonsingular if and only if all diagonal elements of $A$ are non-zero.

The proof for upper triangular matrices is similar except that we would replace columns with rows in our reasoning.