Programming: conditioning and backward stability.

\subsection*{Problem 3.a}
The problem $ H_nx = b $ was considered, where $ H_n $ is the notoriously ill-conditioned Hilbert matrix, with elements $ h_{ij} = 1 / ( i+ j -1 ) $. First, random perturbation of $b$ were generated to study there effects on $x$, and to compare to the condition number $ \kappa(H_n) $ of the matrix. The elements of $b$ were perturbed by $ \pm 1 \times 10^{-15} $. The error in the approximate solution $ \widetilde{x} $ was calculated using $ error = \frac{ \left\Vert \widetilde{x} - x \right\Vert_{\infty} }{ \left\Vert \widetilde{x} \right\Vert_{\infty} } $. The errors and condition numbers are shown below in Table \ref{tab:K} for Hilbert matrices of varying size.

\begin{table}[ht]
	\centering
	\begin{tabular}{|l|l|l|l|l|}
		\hline
		Size (N)  & 2   & 3		& 4	& 5              \\ \hline
		$\kappa(H_n)$ & 27	 & 748  	& 28375 	& 943656   \\ \hline
		Error	 & $5.24 \times 10^{-16} $	& $2.17 \times 10^{-15} $	& $1.21 \times 10^{-14} $	& $4.88 \times 10^{-14} $ \\ \hline
	\end{tabular}
	\caption{Error and condition number for Hilbert system of varying matrix size.}
	\label{tab:K}
\end{table}

It is clear upon examination of Table \ref{tab:K}, that as the size of the Hilbert matrix increases, so do $\kappa(H_n)$ and the associated error of the solution $x$ for small perturbations  of $b$. From Table \ref{tab:K}, by the time $n$ reaches 4, the relative error is already an order of magnitude greater than the initial perturbation. This leads to the conclusion that only systems of $n \leq 3$ are worth solving, because the uncertainty will not get magnified.

The next consideration for the problem $ H_nx = b $ is how $ \kappa(H_n) $ predicts the bounds on the relative error. The desired bound can be determined from the following inequality,

\begin{equation}
\frac{ \left\Vert \delta x \right\Vert_{\infty} }{ \left\Vert x \right\Vert_{\infty} } \leq \kappa( H_n )  \frac{ \left\Vert \delta b \right\Vert_{\infty} }{ \left\Vert b \right\Vert_{\infty} }
\label{eqn:K}
\end{equation}

From Equation \ref{eqn:K}, it is clear that if the right hand side of the equation is perturbed by some amount, say $t$, then both sides of Equation \ref{eqn:K} will be function of $t$. Equation \ref{eqn:K}, shows the new relationship

\begin{equation}
f(t)=\frac{LHS(t)}{RHS(t)}
\leq \kappa( H_n )
\label{eqn:f}
\end{equation}

where $LHS(t)$ and $RHS(t)$ are the functions corresponding to the random perturbation $t$. Equation \ref{eqn:f} is plotted below in Figure \ref{fig:K} for various values of $t$, to see if it will ever exceed $\kappa( H_n )$ for the well conditioned $ 2 \times 2 $ problem.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\textwidth]{figK.png}
	\caption{f(t) plotted as a function of perturbation magnitude t for a 2x2 system.}
	\label{fig:K}
\end{figure}

From Figure \ref{fig:K}, it is clear that $f(t)$ can never exceed $\kappa(H_n)$ since it will always be the upper bound on the relative error. As well, $f(t)$ quickly levels out to approximately $2.4$ for $t > 5 \times 10^{-16}$. We consider this to be the perturbation threshold for which this prediction is sharp. From this we conclude that as the perturbation approached $\varepsilon_{\textnormal{machine}}$ the prediction gets sharper. However, as the size $N$ of the Hilbert matrix increases so too does the upper error bound, in other words, as $\kappa(H_n)$ increases so does the relative error of the solution, which is undesirable.

\subsection*{Problem 3.b}
To empirically verify the need for partial pivoting when small pivot elements appear in the Gaussian elimination process a simple $4 \times 4$ matrix $A$, shown below, was constructed. 

\begin{center}
$ A = 
   \begin{pmatrix} 
      1\times10^{-16}	& 2\times10^{-16}	& 2	& 3 \\ 
      1							& 4						& 6	& 5 \\ 
      3							& 1						& 2	& 3 \\ 
      0							& 5						& 1	& 2
   \end{pmatrix}
$
\end{center}

It should be noted that the matrix is well conditioned since $\kappa(A) = \left\Vert A \right\Vert_{\infty} \cdot \left\Vert A^{-1} \right\Vert_{\infty} \approx 16$, and thus we should expect to be able to solve the problem $Ax = b$ accurately. If $LU$ factorization is performed without partial pivoting, the following result occurs.

\begin{center}
$ LU = 
   \begin{pmatrix} 
      1							& 0		& 0		& 0 \\ 
      1\times10^{16}		& 1		& 0		& 0 \\ 
      3\times10^{16}		& -2.5	& 1		& 0 \\ 
      0							& 2.5	& -0.45	& 1
   \end{pmatrix}
$
$
   \cdot
$
$ 
   \begin{pmatrix} 
      1\times10^{-16}	& 2\times10^{-16}	& 2							& 3 						\\ 
      0							& 2						& -2\times10^{-16}	& -3\times10^{-16}	\\ 
      0							& 0						& -1.1\times10^{-17}	& -1.65\times10^{-17} \\ 
      0							& 0						& 0							& 0
   \end{pmatrix}
$
\end{center}
\begin{center}
$ = 
   \begin{pmatrix} 
      1\times10^{-16}	& 2\times10^{-16}	& 2	& 3 \\ 
      1							& 4						& 8	& 4 \\ 
      3							& 1						& 0	& -16 \\ 
      0							& 5						& 0	& 0
   \end{pmatrix}
$
\end{center}

From the above multiplication it is clear that LU factorization without partial pivoting results in a poor approximation of matrix $A$. This is compared to LU factorization with partial pivoting,

\begin{center}
$ LU = 
   \begin{pmatrix} 
      1								& 0								& 0			& 0 \\ 
      0								& 1								& 0			& 0 \\ 
      0.3333					& 0.7333						& 1			& 0 \\ 
      -3.33\times10^{-17}	&  -3.33\times10^{-17}	& 0.4348	& 1
   \end{pmatrix}
$
$
   \cdot
$
$ 
   \begin{pmatrix} 
      3		& 1	& 2		& 3 			\\ 
      0		& 5	& 1		& 2			\\ 
      0		& 0	& 4.6	& 2.5333	\\ 
      0		& 0	& 0		& 1.8986
   \end{pmatrix} =
$
\end{center}

\begin{center}
$ = PA = 
   \begin{pmatrix} 
      0		& 0		& 1		& 0 \\ 
      0		& 0		& 0		& 1 \\ 
      0		& 1		& 0		& 0 \\ 
      1		& 0		& 0		& 0
   \end{pmatrix}
$
$
\cdot
$
$ 
   \begin{pmatrix} 
      1\times10^{-16}	& 2\times10^{-16}	& 2	& 3 \\ 
      1							& 4						& 6	& 5 \\ 
      3							& 1						& 2	& 3 \\ 
      0							& 5						& 1	& 2
   \end{pmatrix}
$
\end{center}

From the above multiplication it is clear that it is necessary to use partial pivoting when performing LU factorization. It should be noted that partial pivoting adds a negligible amount of computational cost to the overall method of Gaussian elimination since all that is happening is the swapping of rows in the $A$ matrix.