%% Homework 1 Problem 3
clc
clear all

N = 10; % Matrix will be N x N
A = hilb( N );
A_inv = inv( A );
b = zeros(N,1) + 1;
K = cond(A, inf); % Calcluate Condition Number
x = A\b;

% Check how random perturbations of b affect x
error = zeros( 1, 100 );
bound = zeros( 1, 100 );
c = 1e-15;

for i=1:100
    delb = c*rand( size(b) ) - c/2;
    x_til = A\(b + delb);
    del_x = x_til - x;
    error(i) = norm( del_x, inf ) / norm( x_til, inf );
    r = A*x_til - b;
    bound(i) = norm( abs(A_inv)*abs(r), inf ) / norm( x_til, inf );
    
end
ave_err = sum(error) / 100;
ave_bou = sum(bound) / 100;
fprintf( 'Ave Err = %E, Ave Bound = %E \n', ave_err, ave_bou )


% c2 = [ 1.5e-16, 5e-16, 1e-15, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10, 1e-9, 1e1, 1e-2, 1e16 ];
% f = zeros( 1, length(c2) );
% for i=1:length(c2)
%     f1 = zeros( 1, 10000 );
%     for j=1:10000
%         del_b = c2(i)*rand( size(b) ) - c2(i)/2;
%         x_til = A\(b + del_b);
%         del_x = x_til - x;
%         frac1 = norm( del_x, 2 ) / norm( x, 2);
%         frac2 = norm( del_b, 2 ) / norm( b, 2);
%         f1(j) = frac1 / frac2;
%     end
%     f(i) = sum( f1 ) / 10000;
% end
% K1 = zeros( 1, length( c2 ) ) + K;
% semilogx( c2, f, 'b' )
% hold on
% semilogx( c2, K1, 'r--' )
% title( 'f(t) for random perturbation of t magnitude' )
% xlabel( 't' )
% ylabel( 'f(t)' )
% legend( 'f(t)', 'K(A)' )

% %% Part b: Need for partial pivoting
% clc
% clear all
% A = [ 1e-16, 2e-16, 2, 3; 1, 4, 6, 5; 3, 1, 2, 3; 0, 5, 1, 2 ];
% K = cond(A, inf); % Calcluate Condition Number
% % LU factorization
% [Lp, Up, Pp] = lu( A );
% [L, U] = luNoPivot( A );
% 
% % Multiply to Solve for A
% A1 = Lp*Up;
% A2 = L*U;
