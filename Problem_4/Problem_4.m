% MATH 504 Problem 4, computational cost of Gaussian elimination
clc
load eiffel1.mat

%% Setup Positon Vector [ x0, y0 ]^T
% N = length( xnod );
% pos = zeros( 2*N, 1 );
% for i=1:N
%     pos(2*i-1) = xnod(i);
%     pos(2*i)   = ynod(i); 
% end
% 
% % Setup external force vector (b)
% b    = zeros( 2*N, 1 );
% F    = 1/5;
% fpos = 200; % horizontal force node
% b( 2*fpos-1,1 ) = F;
% xcor = xnod( fpos );
% ycor = ynod( fpos );
% 
% % Solve Problem N times using Gaussion Elimination
% t = zeros(1,2);
% for i=1:2
%     tic
%     x = A\b;
%     t(i) = toc;
% end
% 
% % Compute new xnod and ynod
% xload = zeros( N, 1 );
% yload = zeros( N, 1 );
% for i=1:N
%     xload(i) = pos(2*i-1) + x(2*i-1);
%     yload(i) = pos(2*i) + x(2*i); 
% end
% 
% % Compute Average Computation Time for GE alg 
% % ave = sum(t)/length(t);
% 
% % Plot truss results for part a
% trussplot(xnod,ynod,bars)
% hold on
% trussplot(xload,yload,bars)
% plot( xcor, ycor, '*' )
% title( 'Plot of loaded tower versus unloaded tower' )
% xlabel( 'x position' )
% ylabel( 'y position' )

% Plot cpu time vs N for part b
% n = [ 261, 399, 561, 1592 ];
% t = [ 0.00338, 0.010966667, 0.0242, 0.36072];
% loglog( n, t, 'b' )
% title('Loglog plot of CPU time versus N for GE alg')
% xlabel('N')
% ylabel('cpu time (sec)')

%% LU Solve for a single force at each node
% N = length( xnod );
% F = 1/5;
% % Create source vector and solve N times
% b = zeros( 2*N );
% for i=1:N
%     b(2*i-1,2*i-1) = F;
% end
% 
% % Solve Problem by factoring A
% num = 1;
% t = zeros(1,num);
% for j=1:num
%     x = zeros( 2*N );
%     tic
%     [L,U] = lu(A);
%     for i=1:N
%         x(1:2*N,2*i-1)=U\(L\b(1:2*N,2*i-1));
%     end
%     t(j) = toc;
% end
% ave = sum(t)/length(t)
% n = [ 261, 399, 561, 1592 ];
% t = [ 0.28388, 0.96396, 2.208666667, 47.67106667];
% loglog( n, t, 'b' )
% title('Loglog plot of CPU time versus N for GE alg with LU factorization')
% xlabel('N')
% ylabel('cpu time (sec)')

%% Solve using LU factorization realizing A is sparse
N = length( xnod );
F = 1/5;
%spy( A )
A = sparse( A );
N = length( xnod );
F = 1/5;
% Create source vector and solve N times
b = zeros( 2*N );
for i=1:N
    b(2*i-1,2*i-1) = F;
end

% Solve Problem by factoring A
num = 1;
t = zeros(1,num);
for j=1:num
    x = zeros( 2*N );
    tic
    [L,U] = lu(A);
    for i=1:N
        x(1:2*N,2*i-1)=U\(L\b(1:2*N,2*i-1));
    end
    t(j) = toc;
end
ave = sum(t)/length(t)

n = [ 261, 399, 561, 1592 ];
t = [ 0.029166667, 0.0733, 0.177833333, 2.1227];
loglog( n, t, 'b' )
title('CPU time versus N with LU factorization for sparse matrix')
xlabel('N')
ylabel('cpu time (sec)')
